use std::convert::TryFrom;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use crate::pef::Container;

mod pef;
mod ppc;
mod utilities;

fn main() {
    let mut file = File::open(PathBuf::from(concat!("./test_data/deimos_game"))).unwrap();

    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).unwrap();
    let mut container = Container::try_from(buffer.into_boxed_slice()).unwrap();

    container.load_sections().unwrap();
    container
        .sections()
        .iter()
        .for_each(|v| println!("{:?}", v.header()));
    let loader = pef::Loader::try_from(&container.sections()[2]);
    println!("{:?}", loader.unwrap().header());
    let mut data = vec![255; container.sections()[1].header().unpacked_size as usize];
    pef::utilities::init_pattern(&container.sections()[1], &mut data).unwrap();
    println!("{:?}", data);
}
