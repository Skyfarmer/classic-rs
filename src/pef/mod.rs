use std::convert::TryFrom;
use std::rc::Rc;

use crate::pef::raw::{CONTAINER_HEADER_LENGTH, SECTION_HEADER_LENGTH};

mod error;
mod header;
mod raw;
pub mod utilities;

type Error = error::Error;

pub struct Loader<'a> {
    header: raw::LoaderInfoHeader,
    data: &'a [u8],
}

impl<'a> Loader<'a> {
    pub fn header(&self) -> &raw::LoaderInfoHeader {
        &self.header
    }
}

impl<'a> TryFrom<&'a Section> for Loader<'a> {
    type Error = Error;

    fn try_from(section: &'a Section) -> Result<Self, Self::Error> {
        if section.header.kind == header::SectionKind::Loader {
            Ok(Loader {
                header: raw::LoaderInfoHeader::from(&section.data()[..56]),
                data: section.data(),
            })
        } else {
            Err(Error::IncompatibleSection)
        }
    }
}

//TODO Change data field to slice
pub struct Section {
    header: header::SectionHeader,
    data: Rc<[u8]>,
}

impl Section {
    pub fn header(&self) -> &header::SectionHeader {
        &self.header
    }

    pub fn offset(&self) -> usize {
        self.header.offset_container as usize
    }

    pub fn data(&self) -> &[u8] {
        let end = self.offset() + self.header.packed_size as usize;
        &self.data[self.offset()..end]
    }
}

pub struct Container {
    header: header::ContainerHeader,
    sections: Vec<Section>,
    data: Rc<[u8]>,
}

impl Container {
    pub fn load_sections(&mut self) -> Result<(), Error> {
        self.sections.clear();
        for i in 0..self.header.sections {
            self.sections.push(Section {
                header: header::SectionHeader::try_from(
                    &self.data[CONTAINER_HEADER_LENGTH + SECTION_HEADER_LENGTH * i as usize
                        ..CONTAINER_HEADER_LENGTH
                            + SECTION_HEADER_LENGTH
                            + SECTION_HEADER_LENGTH * i as usize],
                )?,
                data: self.data.clone(),
            });
        }
        Ok(())
    }

    pub fn sections(&self) -> &Vec<Section> {
        &self.sections
    }
}

impl TryFrom<Box<[u8]>> for Container {
    type Error = error::Error;

    fn try_from(data: Box<[u8]>) -> Result<Self, Self::Error> {
        Ok(Container {
            header: header::ContainerHeader::try_from(&data[..40])?,
            sections: Vec::new(),
            data: Rc::from(data),
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::utilities::tests::*;

    use super::*;

    #[test]
    fn test_container_layout() {
        let data = load_test_file("deimos_game");
        let mut container = crate::Container::try_from(data.into_boxed_slice()).unwrap();

        container.load_sections().unwrap();
        //Check container
        assert_eq!(container.data.len(), 1106904);
        assert_eq!(container.sections.len(), 3);

        //Check sections
        assert_eq!(container.sections[0].data().len(), 470956);
        assert_eq!(container.sections[0].offset(), 7904);
        assert_eq!(container.sections[0].header.kind, header::SectionKind::Code);
        assert_eq!(container.sections[1].data().len(), 65592);
        assert_eq!(container.sections[1].offset(), 478864);
        assert_eq!(
            container.sections[1].header.kind,
            header::SectionKind::PatternData
        );
        assert_eq!(container.sections[2].data().len(), 7776);
        assert_eq!(container.sections[2].offset(), 128);
        assert_eq!(
            container.sections[2].header.kind,
            header::SectionKind::Loader
        );
    }
}
