use std::fmt::Formatter;

pub enum Error {
    IncompatibleSection,
    IllegalValue {
        field: String,
        actual: String,
        expected: String,
    },
    MismatchedSize(usize, usize),
    UnsupportedArchitecture(u32),
    UnsupportedKind(u8),
    UnsupportedVersion(u32),
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IncompatibleSection => write!(f, "Incompatible section"),
            Self::IllegalValue {
                field,
                actual: a,
                expected: e,
            } => write!(f, "{} invalid, expected {}, got {}", field, e, a),
            Self::MismatchedSize(a, e) => {
                write!(f, "Mismatched size, expected at least {}, got {}", e, a)
            }
            Self::UnsupportedArchitecture(v) => write!(f, "Unsupported architecture: {:x}", v),
            Self::UnsupportedKind(v) => write!(f, "Unsupported kind: {}", v),
            Self::UnsupportedVersion(v) => write!(f, "Unsupported version: {}", v),
        }
    }
}
