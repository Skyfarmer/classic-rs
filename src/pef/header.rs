use std::convert::{TryFrom, TryInto};

//Value is hexadecimal representation of string Joy!
const TAG_JOY: u32 = 0x4a6f_7921;
//Value is hexadecimal representation of string peff
const TAG_PEFF: u32 = 0x7065_6666;

#[derive(PartialEq, Debug)]
pub enum Architecture {
    PowerPc,
}

impl TryFrom<u32> for Architecture {
    type Error = super::error::Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            //Hexadecimal presentation of pwpc
            0x7077_7063 => Ok(Self::PowerPc),
            v => Err(super::error::Error::UnsupportedArchitecture(v)),
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum SectionKind {
    Code,
    UnpackedData,
    PatternData,
    Constant,
    Loader,
    Debug,
    ExecutableData,
    Exception,
    Traceback,
}

impl TryFrom<u8> for SectionKind {
    type Error = super::error::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Code),
            1 => Ok(Self::UnpackedData),
            2 => Ok(Self::PatternData),
            3 => Ok(Self::Constant),
            4 => Ok(Self::Loader),
            5 => Ok(Self::Debug),
            6 => Ok(Self::ExecutableData),
            7 => Ok(Self::Exception),
            8 => Ok(Self::Traceback),
            v => Err(super::error::Error::UnsupportedKind(v)),
        }
    }
}

#[derive(PartialEq, Debug)]
enum ShareKind {
    Process = 1,
    Global = 4,
    Protected = 5,
}

impl TryFrom<u8> for ShareKind {
    type Error = super::error::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            1 => Ok(Self::Process),
            4 => Ok(Self::Global),
            5 => Ok(Self::Protected),
            v => Err(super::error::Error::UnsupportedKind(v)),
        }
    }
}

#[derive(PartialEq, Debug)]
pub struct ContainerHeader {
    pub arch: Architecture,
    pub sections: u16,
}

impl TryFrom<&[u8]> for ContainerHeader {
    type Error = super::error::Error;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Self::try_from(super::raw::ContainerHeader::from(value))
    }
}

impl TryFrom<super::raw::ContainerHeader> for ContainerHeader {
    type Error = super::error::Error;

    fn try_from(value: super::raw::ContainerHeader) -> Result<Self, Self::Error> {
        //Tag 1 must be Joy!
        if value.tag1 != TAG_JOY {
            return Err(super::error::Error::IllegalValue {
                field: "tag2".to_owned(),
                actual: value.tag2.to_string(),
                expected: "peff".to_owned(),
            });
        }
        //tag 2 must be peff
        if value.tag2 != TAG_PEFF {
            return Err(super::error::Error::IllegalValue {
                field: "tag2".to_string(),
                actual: value.tag2.to_string(),
                expected: "peff".to_string(),
            });
        }
        //Only version 1 is supported
        if value.version != 1 {
            return Err(super::error::Error::UnsupportedVersion(value.version));
        }

        Ok(ContainerHeader {
            arch: value.arch.try_into()?,
            sections: value.num_sections,
        })
    }
}

#[derive(PartialEq, Debug)]
pub struct SectionHeader {
    pub kind: SectionKind,
    pub offset_container: u32,
    pub unpacked_size: u32,
    pub packed_size: u32,
}

impl TryFrom<&[u8]> for SectionHeader {
    type Error = super::error::Error;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Self::try_from(super::raw::SectionHeader::from(value))
    }
}

impl TryFrom<super::raw::SectionHeader> for SectionHeader {
    type Error = super::error::Error;

    fn try_from(value: super::raw::SectionHeader) -> Result<Self, Self::Error> {
        Ok(SectionHeader {
            kind: value.kind_section.try_into()?,
            offset_container: value.offset_container,
            unpacked_size: value.unpacked_size,
            packed_size: value.packed_size,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::utilities::tests::*;

    use super::*;

    #[test]
    fn test_container_header_instantiation() {
        let buffer = load_test_file("container_header");
        let actual_header: ContainerHeader = ContainerHeader::try_from(buffer.as_ref()).unwrap();

        let expected_header = ContainerHeader {
            arch: Architecture::PowerPc,
            sections: 3,
        };

        assert_eq!(actual_header, expected_header);
    }

    #[test]
    fn test_section_header_instantiation() {
        let buffer = load_test_file("section_header");
        let actual_header: SectionHeader = SectionHeader::try_from(buffer.as_slice()).unwrap();

        let expected_header = SectionHeader {
            kind: SectionKind::Code,
            offset_container: 0x1ee0,
            unpacked_size: 470956,
            packed_size: 470956,
        };
        assert_eq!(actual_header, expected_header);
    }
}
