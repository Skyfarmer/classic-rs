use std::convert::TryInto;

macro_rules! from_be {
    ( $t:ty, $e:expr) => {
        <$t>::from_be_bytes($e.try_into().unwrap())
    };
}

pub const CONTAINER_HEADER_LENGTH: usize = 40;
pub const SECTION_HEADER_LENGTH: usize = 28;
pub const LOADER_INFO_HEADER_LENGTH: usize = 56;

#[derive(PartialEq, Debug)]
pub struct ContainerHeader {
    pub tag1: u32,
    pub tag2: u32,
    pub arch: u32,
    pub version: u32,
    pub date: u32,
    pub definition_version: u32,
    pub implementation_version: u32,
    pub current_version: u32,
    pub num_sections: u16,
    pub num_inst_sections: u16,
}

impl From<&[u8]> for ContainerHeader {
    fn from(value: &[u8]) -> Self {
        debug_assert_eq!(value.len(), CONTAINER_HEADER_LENGTH);

        ContainerHeader {
            tag1: from_be!(u32, value[0..4]),
            tag2: from_be!(u32, value[4..8]),
            arch: from_be!(u32, value[8..12]),
            version: from_be!(u32, value[12..16]),
            date: from_be!(u32, value[16..20]),
            definition_version: from_be!(u32, value[20..24]),
            implementation_version: from_be!(u32, value[24..28]),
            current_version: from_be!(u32, value[28..32]),
            num_sections: from_be!(u16, value[32..34]),
            num_inst_sections: from_be!(u16, value[34..36]),
        }
    }
}

#[derive(PartialEq, Debug)]
pub struct SectionHeader {
    pub offset_name: i32,
    pub default_address: u32,
    pub size: u32,
    pub unpacked_size: u32,
    pub packed_size: u32,
    pub offset_container: u32,
    pub kind_section: u8,
    pub kind_share: u8,
    pub alignment: u8,
}

impl From<&[u8]> for SectionHeader {
    fn from(value: &[u8]) -> Self {
        debug_assert_eq!(value.len(), SECTION_HEADER_LENGTH);

        SectionHeader {
            offset_name: from_be!(i32, value[0..4]),
            default_address: from_be!(u32, value[4..8]),
            size: from_be!(u32, value[8..12]),
            unpacked_size: from_be!(u32, value[12..16]),
            packed_size: from_be!(u32, value[16..20]),
            offset_container: from_be!(u32, value[20..24]),
            kind_section: value[24],
            kind_share: value[25],
            alignment: value[26],
        }
    }
}

#[derive(PartialEq, Debug)]
pub struct LoaderInfoHeader {
    main_section: i32,
    main_offset: u32,
    init_section: i32,
    init_offset: u32,
    term_section: i32,
    term_offset: u32,
    imported_library_count: u32,
    total_imported_library_count: u32,
    reloc_section_count: u32,
    reloc_instr_offset: u32,
    loader_strings_offset: u32,
    export_hash_offset: u32,
    export_hash_table: u32,
    export_symbol_count: u32,
}

impl From<&[u8]> for LoaderInfoHeader {
    fn from(v: &[u8]) -> Self {
        debug_assert_eq!(v.len(), LOADER_INFO_HEADER_LENGTH);

        LoaderInfoHeader {
            main_section: from_be!(i32, v[0..4]),
            main_offset: from_be!(u32, v[4..8]),
            init_section: from_be!(i32, v[8..12]),
            init_offset: from_be!(u32, v[12..16]),
            term_section: from_be!(i32, v[16..20]),
            term_offset: from_be!(u32, v[20..24]),
            imported_library_count: from_be!(u32, v[24..28]),
            total_imported_library_count: from_be!(u32, v[28..32]),
            reloc_section_count: from_be!(u32, v[32..36]),
            reloc_instr_offset: from_be!(u32, v[36..40]),
            loader_strings_offset: from_be!(u32, v[40..44]),
            export_hash_offset: from_be!(u32, v[44..48]),
            export_hash_table: from_be!(u32, v[48..52]),
            export_symbol_count: from_be!(u32, v[52..56]),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::ops::Deref;

    use crate::utilities::tests::*;

    use super::*;

    #[test]
    fn test_container_header_parsing() {
        let buffer = load_test_file("container_header");

        let actual_header: ContainerHeader = ContainerHeader::from(buffer.deref());
        let expected_header = ContainerHeader {
            tag1: 0x4a6f7921,
            tag2: 0x70656666,
            arch: 0x70777063,
            version: 0x01,
            date: 0xb9c1e540,
            definition_version: 0x00,
            implementation_version: 0x00,
            current_version: 0x00,
            num_sections: 0x03,
            num_inst_sections: 0x02,
        };

        assert_eq!(actual_header, expected_header);
    }

    #[test]
    fn test_section_header_parsing() {
        let buffer = load_test_file("section_header");

        let actual_header: SectionHeader = SectionHeader::from(buffer.as_ref());
        let expected_header = SectionHeader {
            offset_name: -1,
            default_address: 0x00,
            size: 0x072fac,
            unpacked_size: 0x072fac,
            packed_size: 0x072fac,
            offset_container: 0x1ee0,
            kind_section: 0x00,
            kind_share: 0x04,
            alignment: 0x04,
        };

        assert_eq!(actual_header, expected_header);
    }

    #[test]
    fn test_loader_info_header_parsing() {
        let buffer = load_test_file("loader_info_header");

        let actual_header = LoaderInfoHeader::from(buffer.as_ref());
        let expected_header = LoaderInfoHeader {
            main_section: 1,
            main_offset: 6392,
            init_section: -1,
            init_offset: 0,
            term_section: -1,
            term_offset: 0,
            imported_library_count: 10,
            total_imported_library_count: 359,
            reloc_section_count: 1,
            reloc_instr_offset: 1744,
            loader_strings_offset: 2908,
            export_hash_offset: 7768,
            export_hash_table: 1,
            export_symbol_count: 0,
        };

        assert_eq!(actual_header, expected_header)
    }
}
