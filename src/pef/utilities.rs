use crate::pef::Error;

pub fn init_pattern(section: &super::Section, buffer: &mut [u8]) -> Result<(), Error> {
    if section.header().kind == super::header::SectionKind::PatternData {
        if section.header().unpacked_size as usize <= buffer.len() {
            inner_init_pattern(section.data(), buffer)
        } else {
            Err(Error::MismatchedSize(
                buffer.len(),
                section.header().unpacked_size as usize,
            ))
        }
    } else {
        Err(Error::IncompatibleSection)
    }
}

fn inner_init_pattern(mut data: &[u8], mut buffer: &mut [u8]) -> Result<(), Error> {
    loop {
        if data.len() == 0 {
            return Ok(());
        }

        match data[0] >> 5 {
            0 => {
                let (arg_counter, args) = get_arguments(&data, 1);
                let size = args[0] as usize;
                buffer[..size].iter_mut().for_each(|v| *v = 0);

                data = &data[arg_counter..];
                buffer = &mut buffer[size..];
                //inner_init_pattern(&data[arg_counter..], &mut buffer[size..])
            }
            1 => {
                let (arg_counter, args) = get_arguments(&data, 1);
                let size = args[0] as usize;
                buffer[..size].copy_from_slice(&data[arg_counter..arg_counter + size]);

                data = &data[arg_counter + size..];
                buffer = &mut buffer[size..];
                //inner_init_pattern(&data[arg_counter + size..], &mut buffer[size..])
            }
            2 => unimplemented!(),
            t @ 3..=4 => {
                let (arg_counter, args) = get_arguments(&data, 3);
                //Common size as part of the instruction
                let common_size = args[0] as usize;
                //Custom size is second parameter
                let custom_size = args[1] as usize;
                //Repeat count is third parameter
                let repeat_count = args[2] as usize;
                //Zero data for opcode 4
                let zero_data = vec![0; common_size];

                let (custom_offset, common_data) = if t == 3 {
                    //Get common_data
                    (
                        arg_counter + common_size,
                        &data[arg_counter..arg_counter + common_size],
                    )
                } else {
                    //This branch should only be reached if t = 4.
                    //However, the Rust compiler can't verify that t can only be 3 or 4 in all cases
                    //For verification purposes, we leave this debug assertion as a contract.
                    debug_assert_eq!(t, 4);
                    (arg_counter, zero_data.as_slice())
                };

                for i in 0..repeat_count * 2 + 1 {
                    if i % 2 == 0 {
                        //Fill with common pattern
                        let idx = (i / 2) * (common_size + custom_size);
                        buffer[idx..idx + common_size].copy_from_slice(common_data);
                    } else {
                        //Fill with custom pattern
                        let start = custom_offset + (i / 2) * custom_size;
                        let custom_data = &data[start..start + custom_size];

                        let idx = (i / 2) * (common_size + custom_size) + common_size;
                        buffer[idx..idx + custom_size].copy_from_slice(custom_data);
                    }
                }
                //buf_count --> Current starting position
                //(common_size * custom_size) --> Size of each pattern
                //repeat_count --> Times of how many patterns have been written
                //1 --> One additional common pattern at the end
                let buf_end = (common_size + custom_size) * repeat_count + 1;
                let count_end = custom_offset + custom_size * repeat_count;

                data = &data[count_end..];
                buffer = &mut buffer[buf_end..];
                //inner_init_pattern(&data[count_end..], &mut buffer[buf_end..])
            }
            _ => unimplemented!(),
        }
    }
}

fn get_arguments(data: &[u8], mut count: usize) -> (usize, Vec<u32>) {
    let mut args = Vec::new();

    match data[0] & 0x1F {
        0 => (),
        v => {
            count -= 1;
            args.push(v as u32)
        }
    }

    //Variable length encoding
    let mut counter = 0;
    for _ in 0..count {
        let mut tmp: u32 = 0;
        loop {
            counter += 1;
            match data[counter] {
                v if v & 0x80 != 0 => {
                    //More data to come
                    tmp <<= 7;
                    tmp |= v as u32 & 0x7F;
                }
                v => {
                    tmp <<= 7;
                    tmp |= v as u32;
                    break;
                }
            }
        }
        args.push(tmp);
    }

    (counter + 1, args)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::pef::header::{SectionHeader, SectionKind};
    use crate::pef::Section;
    use std::convert::TryInto;
    use std::rc::Rc;

    fn mock_pattern_section(data: Rc<[u8]>, size: u32) -> Section {
        Section {
            header: SectionHeader {
                kind: SectionKind::PatternData,
                offset_container: 0,
                unpacked_size: size,
                packed_size: data.len() as u32,
            },
            data,
        }
    }

    #[test]
    fn get_arguments_single() {
        //Opcode 4 with arg 8
        let test_data = [0x88];
        let (count, args) = get_arguments(&test_data, 1);
        assert_eq!(1, count);
        assert_eq!(args, [0x8])
    }

    #[test]
    fn get_arguments_single_separate_argument() {
        //Opcode 4 with separate arg 8
        let test_data = [0x80, 0x8];
        let (count, args) = get_arguments(&test_data, 1);
        assert_eq!(2, count);
        assert_eq!(args, [0x8])
    }

    #[test]
    fn get_arguments_regular_argument() {
        //Opcode 4 with two args
        let test_data = [0x88, 0x8];
        let (count, args) = get_arguments(&test_data, 2);
        assert_eq!(2, count);
        assert_eq!(args, [0x8, 0x8]);

        //Opcode 4 with two args
        let test_data = [0x80, 0x8, 0x8];
        let (count, args) = get_arguments(&test_data, 2);
        assert_eq!(3, count);
        assert_eq!(args, [0x8, 0x8]);

        //Opcode 4 with three args
        let test_data = [0x80, 0x0, 0x8, 0x88, 0x8];
        let (count, args) = get_arguments(&test_data, 3);
        assert_eq!(5, count);
        assert_eq!(args, [0x0, 0x8, 0x408]);
    }

    #[test]
    fn init_zero_pattern() {
        let pattern = mock_pattern_section(Rc::from([0x5]), 5);
        let mut buffer = vec![255; 5];
        init_pattern(&pattern, &mut buffer).unwrap();

        assert_eq!(buffer, [0; 5])
    }

    #[test]
    fn init_block_copy_pattern() {
        let data: Rc<[u8]> = Rc::from([0x25, 0x88, 0x87, 0x86, 0x85, 0x84]);
        let pattern = mock_pattern_section(data.clone(), 5);
        let mut buffer = vec![255; 5];

        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer.as_slice(), &data[1..])
    }

    #[test]
    fn init_interleave_block_copy_pattern() {
        //1 Byte common - 1 time 1 Byte custom
        let pattern = mock_pattern_section(Rc::from([0x61, 0x1, 0x1, 0x88, 0x77]), 3);
        let mut buffer = vec![255; 3];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer, [0x88, 0x77, 0x88]);

        //3 Byte common - 1 time 1 Byte custom
        let pattern = mock_pattern_section(Rc::from([0x63, 0x1, 0x1, 0x88, 0x87, 0x86, 0x77]), 7);
        let mut buffer = vec![255; 7];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer, [0x88, 0x87, 0x86, 0x77, 0x88, 0x87, 0x86]);

        //1 Byte common - 1 time 2 Byte custom
        let pattern = mock_pattern_section(Rc::from([0x61, 0x2, 0x1, 0x88, 0x77, 0x76]), 4);
        let mut buffer = vec![255; 4];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer, [0x88, 0x77, 0x76, 0x88]);

        //1 Byte common - 2 times 2 Byte custom
        let pattern =
            mock_pattern_section(Rc::from([0x61, 0x2, 0x2, 0x88, 0x77, 0x76, 0x75, 0x74]), 7);
        let mut buffer = vec![255; 7];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer, [0x88, 0x77, 0x76, 0x88, 0x75, 0x74, 0x88]);

        //2 Byte common - 2 times 3 Byte custom
        let pattern = mock_pattern_section(
            Rc::from([
                0x62, 0x3, 0x2, 0x88, 0x87, 0x77, 0x76, 0x75, 0x74, 0x73, 0x72,
            ]),
            12,
        );
        let mut buffer = vec![255; 12];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(
            buffer,
            [0x88, 0x87, 0x77, 0x76, 0x75, 0x88, 0x87, 0x74, 0x73, 0x72, 0x88, 0x87]
        );
    }

    #[test]
    fn init_interleave_zero_pattern() {
        //1 Byte common - 1 time 1 Byte custom
        let pattern = mock_pattern_section(Rc::from([0x81, 0x1, 0x1, 0x77]), 3);
        let mut buffer = vec![255; 3];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer, [0x0, 0x77, 0x0]);

        //3 Byte common - 1 time 1 Byte custom
        let pattern = mock_pattern_section(Rc::from([0x83, 0x1, 0x1, 0x77]), 7);
        let mut buffer = vec![255; 7];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(buffer, [0x0, 0x0, 0x0, 0x77, 0x0, 0x0, 0x0]);

        //1 Byte common - 3 times 2 Byte custom
        let pattern = mock_pattern_section(
            Rc::from([0x81, 0x2, 0x3, 0x77, 0x76, 0x75, 0x74, 0x73, 0x72]),
            10,
        );
        let mut buffer = vec![255; 10];
        init_pattern(&pattern, &mut buffer).unwrap();
        assert_eq!(
            buffer,
            [0x0, 0x77, 0x76, 0x0, 0x75, 0x74, 0x0, 0x73, 0x72, 0x0]
        );
    }
}
