#[cfg(test)]
pub mod tests {
    use std::fs::File;

    pub fn load_test_file(name: &str) -> Vec<u8> {
        use std::io::Read;

        let mut file = get_test_file(name);

        let mut buffer = Vec::new();
        file.read_to_end(&mut buffer).unwrap();

        buffer
    }

    pub fn get_test_file(name: &str) -> File {
        use std::path::PathBuf;

        File::open(PathBuf::from(format!(
            "{}/test_data/{}",
            env!("CARGO_MANIFEST_DIR"),
            name
        )))
        .unwrap()
    }
}
