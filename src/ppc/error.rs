pub enum Error {
    IllegalOperand(String),
    UnknownBranchMnemonic(u8, u8),
    UnknownOpcode(u8),
}
