use std::convert::{TryFrom, TryInto};

use crate::pef::Section;

mod error;
mod isa;

const LESS_THAN: u8 = 0;
const GREATER_THAN: u8 = 1;
const EQUAL: u8 = 2;
const SUM_OVERFLOW: u8 = 3;

macro_rules! is_branch {
    ( BDNZ => $e:expr) => {
        is_branch!($e, 16, 0b10110)
    };
    ( BDNZF => $e:expr) => {
        is_branch!($e, 0, 0b11110)
    };
    ( BDNZT => $e:expr) => {
        is_branch!($e, 8, 0b11110)
    };
    ( BDZ => $e:expr ) => {
        is_branch!($e, 18, 0b10110)
    };
    ( BDZF => $e:expr) => {
        is_branch!($e, 2, 0b11110)
    };
    ( BDZT => $e:expr) => {
        is_branch!($e, 10, 0b11110)
    };
    ( BDZL => $e:expr) => {
        is_branch!(BDZ => $e)
    };
    ( BDZTL => $e:expr) => {
        is_branch!(BDZT => $e)
    };
    ( BEQ => $e:expr, $f:expr) => {
        is_branch!($e, $f, 12, 0b11100, EQUAL)
    };
    ( BEQLR => $e:expr, $f:expr) => {
        is_branch!(BEQ => $e, $f)
    };
    ( BGE => $e:expr, $f:expr ) => {
        is_branch!($e, $f, 4, 0b11100, LESS_THAN)
    };
    ( BGELR => $e:expr, $f:expr ) => {
        is_branch!(BGE => $e, $f)
    };
    ( BGT => $e:expr, $f:expr ) => {
        is_branch!($e, $f, 12, 0b11100, GREATER_THAN)
    };
    ( BGTLR => $e:expr, $f:expr ) => {
        is_branch!(BGT => $e,$f)
    };
    ( BLE => $e:expr, $f:expr) => {
        is_branch!($e, $f, 4, 0b11100, GREATER_THAN)
    };
    ( BLELR => $e:expr, $f:expr) => {
        is_branch!(BLE => $e, $f)
    };
    ( BLR => $e:expr) => {
        is_branch!($e , 20, 0b10100)
    };
    ( BLT => $e:expr, $f:expr) => {
        is_branch!($e, $f, 12, 0b11100, LESS_THAN)
    };
    ( BLTLR => $e:expr, $f:expr) => {
        is_branch!(BLT => $e, $f)
    };
    ( BNE => $e:expr, $f:expr) => {
        is_branch!($e, $f, 4, 0b11100, EQUAL)
    };
    ( BNELR => $e:expr, $f:expr) => {
        is_branch!(BNE => $e, $f)
    };
    ( BNS => $e:expr, $f:expr) => {
        is_branch!($e, $f, 4, 0b11100, SUM_OVERFLOW)
    };
    ( $e:expr, $g:expr, $mask:expr) => {
        $e & $mask == $g
    };
    ( $e:expr, $f:expr, $g:expr, $mask:expr, $h:expr) => {
        is_branch!($e,$g, $mask) && $f & 0x3 == $h
    };
}

enum Branch {
    BaseCond(u8, u8, Option<bool>),
    BaseCondLink(u8, u8, Option<bool>),
    BDNZ(u8, Option<bool>),
    BDNZF(u8, Option<bool>),
    BDNZT(u8, Option<bool>),
    BDZF(u8),
    BDZT(u8),
    BDZTL(u8),
    BEQ(u8, Option<bool>),
    BEQLR(u8),
    BGE(u8, Option<bool>),
    BGELR(u8),
    BGT(u8, Option<bool>),
    BGTLR(u8),
    BLE(u8, Option<bool>),
    BLELR(u8),
    BLR,
    BLT(u8, Option<bool>),
    BLTLR(u8),
    BNE(u8, Option<bool>),
    BNELR(u8),
    BNS(u8, Option<bool>),
}

impl Branch {
    fn get_hint(value: u8, mask: u8) -> Option<bool> {
        match value & mask {
            0 => None,
            1 => None,
            v => Some(v & 0x1 == 1),
        }
    }

    fn cr2str(value: u8) -> &'static str {
        match value % 4 {
            LESS_THAN => "lt",
            GREATER_THAN => "gt",
            EQUAL => "eq",
            SUM_OVERFLOW => "so",
            _ => unreachable!(),
        }
    }

    fn format_cond_register(value: u8, extended: bool, separator: bool) -> String {
        let cond_sep = |v| if v { "," } else { "" };
        match value {
            v if v / 4 > 0 => {
                if extended {
                    format!("4*cr{}+{}{}", v / 4, Self::cr2str(v), cond_sep(separator))
                } else {
                    format!("cr{}{}", v / 4, cond_sep(separator))
                }
            }
            v => {
                if extended {
                    format!("{}{}", Self::cr2str(v), cond_sep(separator))
                } else {
                    "".to_string()
                }
            }
        }
    }
}

impl std::fmt::Display for Branch {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let check_hint = |v| match v {
            Some(true) => "+",
            Some(false) => "-",
            None => "",
        };

        match *self {
            Self::BaseCond(bo, bi, hint) => write!(
                f,
                "bc{} {},{}",
                check_hint(hint),
                bo,
                Self::format_cond_register(bi, true, true)
            ),
            Self::BaseCondLink(bo, bi, hint) => write!(
                f,
                "bcl{} {},{}",
                check_hint(hint),
                bo,
                Self::format_cond_register(bi, true, true)
            ),
            Self::BDNZ(_, hint) => write!(f, "bdnz{} ", check_hint(hint)),
            Self::BDNZF(v, _) => write!(f, "bdnzf {}", Self::format_cond_register(v, true, true)),
            Self::BDNZT(v, _) => write!(f, "bdnzt {}", Self::format_cond_register(v, true, true)),
            Self::BDZF(v) => write!(f, "bdzf {}", Self::format_cond_register(v, true, true)),
            Self::BDZT(v) => write!(f, "bdzt {}", Self::format_cond_register(v, true, true)),
            Self::BDZTL(v) => write!(f, "bdztl {}", Self::format_cond_register(v, true, true)),
            Self::BEQ(_, hint) => write!(f, "beq{} ", check_hint(hint)),
            Self::BEQLR(v) => write!(f, "beqlr {}", Self::format_cond_register(v, false, false)),
            Self::BGE(_, hint) => write!(f, "bge{} ", check_hint(hint)),
            Self::BGELR(v) => write!(f, "bgelr {}", Self::format_cond_register(v, false, false)),
            Self::BGT(_, hint) => write!(f, "bgt{} ", check_hint(hint)),
            Self::BGTLR(v) => write!(f, "bgtlr {}", Self::format_cond_register(v, false, false)),
            Self::BLE(_, hint) => write!(f, "ble{} ", check_hint(hint)),
            Self::BLELR(v) => write!(f, "blelr {}", Self::format_cond_register(v, false, false)),
            Self::BLR => write!(f, "blr"),
            Self::BLT(v, hint) => write!(
                f,
                "blt{} {}",
                check_hint(hint),
                Self::format_cond_register(v, false, true)
            ),
            Self::BLTLR(v) => write!(f, "bltlr {}", Self::format_cond_register(v, false, false)),
            Self::BNE(_, hint) => write!(f, "bne{} ", check_hint(hint)),
            Self::BNELR(v) => write!(f, "bnelr {}", Self::format_cond_register(v, false, false)),
            Self::BNS(v, hint) => write!(
                f,
                "bns{} {}",
                check_hint(hint),
                Self::format_cond_register(v, false, true)
            ),
        }
    }
}

impl TryFrom<isa::ISA> for Branch {
    type Error = error::Error;

    fn try_from(value: isa::ISA) -> Result<Self, Self::Error> {
        use isa::ISA::*;
        match value {
            BC(bo, bi, _) => {
                if bi / 4 == 0 && is_branch!(BDNZ => bo) {
                    Ok(Self::BDNZ(bi, Self::get_hint(bo, 0b01001)))
                } else if is_branch!(BDNZ => bo) {
                    Ok(Self::BaseCond(bo, bi, Self::get_hint(bo, 0b01001)))
                } else if bi / 4 == 0 && is_branch!(BDNZF => bo) {
                    Ok(Self::BDNZF(bi, None))
                } else if bi / 4 == 0 && is_branch!(BDNZT => bo) {
                    Ok(Self::BDNZT(bi, None))
                } else if is_branch!(BDZ => bo) {
                    Ok(Self::BaseCond(bo, bi, Self::get_hint(bo, 0b01001)))
                } else if is_branch!(BDZF => bo) {
                    Ok(Self::BDZF(bi))
                } else if is_branch!(BDZT => bo) {
                    Ok(Self::BDZT(bi))
                } else if is_branch!(BEQ => bo, bi) {
                    Ok(Self::BEQ(bi, Self::get_hint(bo, 0b11)))
                } else if is_branch!(BGE => bo, bi) {
                    Ok(Self::BGE(bi, Self::get_hint(bo, 0b11)))
                } else if is_branch!(BGT => bo, bi) {
                    Ok(Self::BGT(bi, None))
                } else if is_branch!(BLE => bo, bi) {
                    Ok(Self::BLE(bi, None))
                } else if is_branch!(BLT => bo, bi) {
                    Ok(Self::BLT(bi, Self::get_hint(bo, 0b11)))
                } else if is_branch!(BNE => bo, bi) {
                    Ok(Self::BNE(bi, None))
                } else if is_branch!(BNS => bo, bi) {
                    Ok(Self::BNS(bi, Self::get_hint(bo, 0b11)))
                } else {
                    Err(Self::Error::UnknownBranchMnemonic(bo, bi))
                }
            }
            BCL(bo, bi, _) => {
                if is_branch!(BDZL => bo) {
                    Ok(Self::BaseCondLink(bo, bi, Self::get_hint(bo, 0b01001)))
                } else if is_branch!(BDZTL => bo) {
                    Ok(Self::BDZTL(bi))
                } else {
                    Err(Self::Error::UnknownBranchMnemonic(bo, bi))
                }
            }
            BCLR(bo, bi, _) => {
                if is_branch!(BEQLR => bo, bi) {
                    Ok(Self::BEQLR(bi))
                } else if is_branch!(BGELR => bo, bi) {
                    Ok(Self::BGELR(bi))
                } else if is_branch!(BGTLR => bo, bi) {
                    Ok(Self::BGTLR(bi))
                } else if is_branch!(BLELR => bo, bi) {
                    Ok(Self::BLELR(bi))
                } else if is_branch!(BLR => bo) {
                    Ok(Self::BLR)
                } else if is_branch!(BLTLR => bo, bi) {
                    Ok(Self::BLTLR(bi))
                } else if is_branch!(BNELR => bo, bi) {
                    Ok(Self::BNELR(bi))
                } else {
                    Err(Self::Error::UnknownBranchMnemonic(bo, bi))
                }
            }
            _ => unimplemented!(),
        }
    }
}

pub struct Decoder<'a> {
    section: &'a Section,
}

impl<'a> Decoder<'a> {
    fn get(&self, position: usize) -> u32 {
        let word = self.section.data().chunks(4).nth(position).unwrap();
        u32::from_be_bytes(word.try_into().unwrap())
    }

    fn decode(&self, position: usize) -> Result<isa::ISA, error::Error> {
        isa::ISA::try_from(self.get(position))
    }

    fn decode_string(&self, position: usize) -> Result<String, error::Error> {
        use isa::ISA::*;
        let rel2abs = |imm| (imm << 2) + position as i32 * 4 + self.section.offset() as i32;
        let cond_zero = |v, instead: &str| {
            if v == 0 {
                instead.to_string()
            } else {
                format!("r{}", v)
            }
        };
        macro_rules! format_rcd {
          ( $arg:tt, $cond:expr, $($e:expr),*) => {
                 format!($arg, if $cond { "." } else { "" }, $($e),*)
            };
        };

        Ok(match self.decode(position)? {
            ATTN => "attn".to_string(),
            ADD(r1, r2, r3, rcd) => format_rcd!("add{} r{},r{},r{}", rcd, r1, r2, r3),
            ADDC(r1, r2, r3, _) => format!("addc r{},r{},r{}", r1, r2, r3),
            ADDCO(_, _, _, _) => unimplemented!(),
            ADDE(r1, r2, r3, _) => format!("adde r{},r{},r{}", r1, r2, r3),
            ADDEO(_, _, _, _) => unimplemented!(),
            ADDI(r1, r2, c) => {
                if r2 == 0 {
                    format!("li r{},{}", r1, c)
                } else {
                    format!("addi r{},r{},{}", r1, r2, c)
                }
            }
            ADDIC(r1, r2, v, rcd) => format_rcd!("addic{} r{},r{},{}", rcd, r1, r2, v),
            ADDIS(r1, r2, v) => {
                if r2 == 0 {
                    format!("lis r{},{}", r1, v)
                } else {
                    format!("addis r{},r{},{}", r1, r2, v)
                }
            }
            ADDO(_, _, _, _) => unimplemented!(),
            ADDPCIS(r1, v) => format!("addpcis r{},{}", r1, v),
            ADDZE(r1, r2, rcd) => format_rcd!("addze{} r{},r{}", rcd, r1, r2),
            ADDZEO(_, _, _) => unimplemented!(),
            AND(r1, r2, r3, rcd) => format_rcd!("and{} r{},r{},r{}", rcd, r2, r1, r3),
            ANDC(r1, r2, r3, _) => format!("andc r{},r{},r{}", r2, r1, r3),
            ANDI(r1, r2, v) => format!("andi. r{},r{},{}", r2, r1, v),
            ANDIS(r1, r2, v) => format!("andis. r{},r{},{}", r2, r1, v),
            B(imm) => format!("b {:x}", rel2abs(imm)),
            BA(imm) => format!("ba {:x}", imm << 2),
            BC(bo, bi, imm) => {
                let branch = Branch::try_from(BC(bo, bi, imm))?;
                format!("{}{:x}", branch, rel2abs(imm))
            }
            BCA(bo, bi, imm) => format!(
                "bca {},{}{:x}",
                bo,
                Branch::format_cond_register(bi, true, true),
                imm << 2
            ),
            BCCTR(_, _, _) => "bctr".to_string(),
            BCL(bo, bi, imm) => {
                let branch = Branch::try_from(BCL(bo, bi, imm))?;
                format!("{}{:x}", branch, rel2abs(imm))
            }
            BCLA(_, _, _) => unimplemented!(),
            BCLR(bo, bi, imm) => Branch::try_from(BCLR(bo, bi, imm))?
                .to_string()
                .trim_end()
                .to_string(),
            BCLRL(_, _, _) => unimplemented!(),
            BL(imm) => format!("bl {:x}", rel2abs(imm)),
            BLA(imm) => format!("bla {:x}", imm << 2),
            CMP(_, flag, r1, r2) => {
                if flag {
                    unimplemented!()
                } else {
                    format!("cmpw r{},r{}", r1, r2)
                }
            }
            CMPI(_, flag, r1, r2) => {
                if flag {
                    unimplemented!()
                } else {
                    format!("cmpwi r{},{}", r1, r2)
                }
            }
            CMPLI(bf, flag, r1, v) => {
                let cr = if bf != 0 {
                    format!("cr{},", bf)
                } else {
                    "".to_string()
                };
                if flag {
                    format!("cmpldi {}r{},{}", cr, r1, v)
                } else {
                    format!("cmplwi {}r{},{}", cr, r1, v)
                }
            }
            CMPL(_, flag, r1, r2) => {
                if flag {
                    unimplemented!()
                } else {
                    format!("cmplw r{},r{}", r1, r2)
                }
            }
            CNTLZW(r1, r2, _) => format!("cntlzw r{},r{}", r2, r1),
            CROR(cr1, cr2, cr3) => format!(
                "cror {},{},{}",
                Branch::cr2str(cr1),
                Branch::cr2str(cr2),
                Branch::cr2str(cr3)
            ),
            DIVW(r1, r2, r3, rcd) => format_rcd!("divw{} r{},r{},r{}", rcd, r1, r2, r3),
            DIVWO(_, _, _, _) => unimplemented!(),
            DIVWU(r1, r2, r3, _) => format!("divwu r{},r{},r{}", r1, r2, r3),
            DIVWUO(_, _, _, _) => unimplemented!(),
            EXTSB(r1, r2, rcd) => format_rcd!("extsb{} r{},r{}", rcd, r2, r1),
            EXTSH(r1, r2, rcd) => format_rcd!("extsh{} r{},r{}", rcd, r2, r1),
            FABS(f1, f2, _) => format!("fabs f{},f{}", f1, f2),
            FADD(f1, f2, f3, _) => format!("fadd f{},f{},f{}", f1, f2, f3),
            FADDS(f1, f2, f3, _) => format!("fadds f{},f{},f{}", f1, f2, f3),
            FCMPO(cr, f1, f2) => format!("fcmpo cr{},f{},f{}", cr, f1, f2),
            FCTIWZ(r1, r2, _) => format!("fctiwz f{},f{}", r1, r2),
            FCMPU(cr, f1, f2) => format!("fcmpu cr{},f{},f{}", cr, f1, f2),
            FDIV(f1, f2, f3, _) => format!("fdiv f{},f{},f{}", f1, f2, f3),
            FDIVS(f1, f2, f3, _) => format!("fdivs f{},f{},f{}", f1, f2, f3),
            FMADD(f1, f2, f3, f4, _) => format!("fmadd f{},f{},f{},f{}", f1, f2, f4, f3),
            FMADDS(f1, f2, f3, f4, _) => format!("fmadds f{},f{},f{},f{}", f1, f2, f4, f3),
            FMR(f1, f2, _) => format!("fmr f{},f{}", f1, f2),
            FMUL(f1, f2, f3, _) => format!("fmul f{},f{},f{}", f1, f2, f3),
            FMULS(f1, f2, f3, _) => format!("fmuls f{},f{},f{}", f1, f2, f3),
            FMSUBS(f1, f2, f3, f4, _) => format!("fmsubs f{},f{},f{},f{}", f1, f2, f4, f3),
            FNEG(f1, f2, _) => format!("fneg f{},f{}", f1, f2),
            FNMADD(f1, f2, f3, f4, rcd) => {
                format_rcd!("fnmadd{} f{},f{},f{},f{}", rcd, f1, f2, f4, f3)
            }
            FNMSUB(f1, f2, f3, f4, _) => format!("fnmsub f{},f{},f{},f{}", f1, f2, f4, f3),
            FNMSUBS(f1, f2, f3, f4, _) => format!("fnmsubs f{},f{},f{},f{}", f1, f2, f4, f3),
            FRSP(f1, f2, _) => format!("frsp f{},f{}", f1, f2),
            FSUB(f1, f2, f3, _) => format!("fsub f{},f{},f{}", f1, f2, f3),
            FSUBS(f1, f2, f3, _) => format!("fsubs f{},f{},f{}", f1, f2, f3),
            LBZ(r1, r2, imm) => format!("lbz r{},{}(r{})", r1, imm, r2),
            LBZU(r1, r2, v) => format!("lbzu r{},{}(r{})", r1, v, r2),
            LBZX(r1, r2, r3) => format!("lbzx r{},r{},r{}", r1, r2, r3),
            LD(r1, r2, v) => format!("ld r{},{}({})", r1, v << 2, cond_zero(r2, "0")),
            LFD(f1, r1, v) => format!("lfd f{},{}({})", f1, v, cond_zero(r1, "0")),
            LFDU(f1, r1, v) => format!("lfdu f{},{}(r{})", f1, v, r1),
            LFS(f1, f2, v) => format!("lfs f{},{}({})", f1, v, cond_zero(f2, "0")),
            LFSU(f1, r1, v) => format!("lfsu f{},{}(r{})", f1, v, r1),
            LFSX(f1, r1, r2) => format!("lfsx f{},r{},r{}", f1, r1, r2),
            LHA(r1, r2, v) => format!("lha r{},{}({})", r1, v, cond_zero(r2, "0")),
            LHAX(r1, r2, r3) => format!("lhax r{},r{},r{}", r1, r2, r3),
            LHZX(r1, r2, r3) => format!("lhzx r{},r{},r{}", r1, r2, r3),
            LMW(r1, r2, imm) => format!("lmw r{},{}(r{})", r1, imm, r2),
            LHZ(r1, r2, v) => format!("lhz r{},{}(r{})", r1, v, r2),
            LVEWX(v1, v, r1) => format!("lvewx v{},{},r{}", v1, v, r1),
            LVX(v1, v, r1) => format!("lvx v{},{},r{}", v1, v, r1),
            LWARX(r1, r2, r3, _) => format!("lwarx r{},{},r{}", r1, cond_zero(r2, "0"), r3),
            LWZ(r1, r2, c) => format!("lwz r{},{}({})", r1, c, cond_zero(r2, "0")),
            LWZU(r1, r2, v) => format!("lwzu r{},{}(r{})", r1, v, r2),
            LWZX(r1, r2, r3) => format!("lwzx r{},r{},r{}", r1, r2, r3),
            MFCR(r) => format!("mfcr r{}", r),
            MFOCRF(_, _) => unimplemented!(),
            MFSPR(r, spr) => match spr {
                4 => format!("mfrtcu r{}", r),
                5 => format!("mfrtcl r{}", r),
                8 => format!("mflr r{}", r),
                256 => format!("mfvrsave r{}", r),
                268 => format!("mftb r{}", r),
                269 => format!("mftbu r{}", r),
                _ => format!("mfspr {},r{}", spr, r),
            },
            MFVSCR(v) => format!("mfvscr v{}", v),
            MTSPR(r, spr) => match spr {
                8 => format!("mtlr r{}", r),
                9 => format!("mtctr r{}", r),
                256 => format!("mtvrsave r{}", r),
                _ => format!("mtspr {},r{}", spr, r),
            },
            MULHW(r1, r2, r3, _) => format!("mulhw r{},r{},r{}", r1, r2, r3),
            MULHWU(r1, r2, r3, _) => format!("mulhwu r{},r{},r{}", r1, r2, r3),
            MULLI(r1, r2, r3) => format!("mulli r{},r{},{}", r1, r2, r3),
            MULLW(r1, r2, r3, rcd) => format_rcd!("mullw{} r{},r{},r{}", rcd, r1, r2, r3),
            MULLWO(_, _, _, _) => format!(""),
            MTCRF(r1, mask) => {
                if mask == 0xFF {
                    format!("mtcr r{}", r1)
                } else {
                    unimplemented!()
                }
            }
            MTOCRF(_, _) => unimplemented!(),
            MTVSCR(v) => format!("mtvscr v{}", v),
            NEG(r1, r2, rcd) => format_rcd!("neg{} r{},r{}", rcd, r1, r2),
            NEGO(_, _, _) => unimplemented!(),
            NOR(r1, r2, r3, _) => {
                if r1 == r3 {
                    format!("not r{},r{}", r2, r1)
                } else {
                    format!("nor r{},r{},r{}", r2, r1, r3)
                }
            }
            OR(r1, r2, r3, rcd) => {
                if r1 == r3 {
                    format_rcd!("mr{} r{},r{}", rcd, r2, r1)
                } else {
                    format_rcd!("or{} r{},r{},r{}", rcd, r2, r1, r3)
                }
            }
            ORI(r1, r2, imm) => {
                if r1 == 0 && r2 == 0 && imm == 0 {
                    "nop".to_string()
                } else {
                    format!("ori r{},r{},{}", r2, r1, imm)
                }
            }
            ORIS(r1, r2, v) => format!("oris r{},r{},{}", r2, r1, v),
            RLDIC(r1, r2, sh, mb, rcd) => format_rcd!("rldic{} r{},r{},{},{}", rcd, r2, r1, sh, mb),
            RLDICL(r1, r2, sh, mb, rcd) => {
                if mb == 0 {
                    format!("rotldi r{},r{},{}", r2, r1, sh)
                } else {
                    format_rcd!("rldicl{} r{},r{},{},{}", rcd, r2, r1, sh, mb)
                }
            }
            RLDICR(_, _, _, _, _) => unimplemented!(),
            RLDIMI(_, _, _, _, _) => unimplemented!(),
            RLWIMI(r1, r2, v1, v2, v3, rcd) => {
                format_rcd!("rlwimi{} r{},r{},{},{},{}", rcd, r2, r1, v1, v2, v3)
            }
            RLWINM(r1, r2, sh, value, me, rcd) => {
                if value == 0 && me == 31 {
                    format!("rotlwi r{},r{},{}", r2, r1, sh)
                } else if sh == 0 && me == 31 {
                    format_rcd!("clrlwi{} r{},r{},{}", rcd, r2, r1, value)
                } else {
                    format_rcd!("rlwinm{} r{},r{},{},{},{}", rcd, r2, r1, sh, value, me)
                }
            }
            SLW(r1, r2, r3, _) => format!("slw r{},r{},r{}", r2, r1, r3),
            SRW(r1, r2, r3, _) => format!("srw r{},r{},r{}", r2, r1, r3),
            SRAWI(r1, r2, v, _) => format!("srawi r{},r{},{}", r2, r1, v),
            STB(r1, r2, imm) => format!("stb r{},{}({})", r1, imm, cond_zero(r2, "0")),
            STBU(r1, r2, v) => format!("stbu r{},{}(r{})", r1, v, r2),
            STBX(r1, r2, r3) => format!("stbx r{},r{},r{}", r1, r2, r3),
            STD(r1, r2, v) => format!("std r{},{}({})", r1, v, cond_zero(r2, "0")),
            STFD(r1, r2, v) => format!("stfd f{},{}(r{})", r1, v, r2),
            STFDU(f1, r1, v) => format!("stfdu f{},{}(r{})", f1, v, r1),
            STFDX(f1, r1, r2) => format!("stfdx f{},r{},r{}", f1, r1, r2),
            STFSX(f1, r1, r2) => format!("stfsx f{},r{},r{}", f1, r1, r2),
            STFS(f1, r1, v) => format!("stfs f{},{}({})", f1, v, cond_zero(r1, "0")),
            STH(r1, r2, v) => format!("sth r{},{}(r{})", r1, v, r2),
            STHBRX(r1, r2, r3) => format!("sthbrx r{},{},r{}", r1, cond_zero(r2, "0"), r3),
            STHX(r1, r2, r3) => format!("sthx r{},r{},r{}", r1, r2, r3),
            STVEWX(v1, v, r1) => format!("stvewx v{},{},r{}", v1, v, r1),
            STVX(v1, r1, r2) => format!("stvx v{},r{},r{}", v1, r1, r2),
            STMW(r1, r2, v) => format!("stmw r{},{}(r{})", r1, v, r2),
            STW(r1, r2, v) => format!("stw r{},{}({})", r1, v, cond_zero(r2, "0")),
            STWBRX(r1, r2, r3) => format!("stwbrx r{},{},r{}", r1, cond_zero(r2, "0"), r3),
            STWCX(r1, r2, r3) => format!("stwcx. r{},{},r{}", r1, cond_zero(r2, "0"), r3),
            STWU(r1, r2, v) => format!("stwu r{},{}(r{})", r1, v, r2),
            STWX(r1, r2, r3) => format!("stwx r{},r{},r{}", r1, r2, r3),
            SUBF(r1, r2, r3, rcd) => format_rcd!("subf{} r{},r{},r{}", rcd, r1, r2, r3),
            SUBFC(r1, r2, r3, _) => format!("subfc r{},r{},r{}", r1, r2, r3),
            SUBFCO(_, _, _, _) => format!(""),
            SUBFE(r1, r2, r3, rcd) => format_rcd!("subfe{} r{},r{},r{}", rcd, r1, r2, r3),
            SUBFEO(_, _, _, _) => unimplemented!(),
            SUBFIC(r1, r2, v) => format!("subfic r{},r{},{}", r1, r2, v),
            SUBFO(_, _, _, _) => unimplemented!(),
            SUBFZE(r1, r2, _) => format!("subfze r{},r{}", r1, r2),
            SUBFZEO(_, _, _) => unimplemented!(),
            SYNC(t) => match t {
                0 => "hwsync".to_string(),
                1 => "lwsync".to_string(),
                2 => "ptesync".to_string(),
                _ => unimplemented!(),
            },
            TDI(to, r1, v) => match to {
                2 => format!("tdllti r{},{}", r1, v),
                4 => format!("tdeqi r{},{}", r1, v),
                12 => format!("tdgei r{},{}", r1, v),
                _ => format!("tdi {},r{},{}", to, r1, v),
            },
            VADDUBM(v1, v2, v3) => format!("vaddubm v{},v{},v{}", v1, v2, v3),
            VMHRADDSHS(v1, v2, v3, v4) => format!("vmhraddshs v{},v{},v{},v{}", v1, v2, v3, v4),
            XOR(r1, r2, r3, rcd) => format_rcd!("xor{} r{},r{},r{}", rcd, r2, r1, r3),
            XORI(r1, r2, v) => {
                if r1 == 0 && r2 == 0 && v == 0 {
                    "xnop".to_string()
                } else {
                    format!("xori r{},r{},{}", r2, r1, v)
                }
            }
            XORIS(r1, r2, v) => format!("xoris r{},r{},{}", r2, r1, v),
        })
    }
}

impl<'a> From<&'a crate::pef::Section> for Decoder<'a> {
    fn from(section: &'a Section) -> Self {
        Decoder { section }
    }
}

impl<'a> Into<String> for Decoder<'a> {
    fn into(self) -> String {
        let mut output = String::new();
        for i in 0..self.section.data().chunks(4).len() {
            if let Ok(v) = self.decode_string(i) {
                output.push_str(&v);
            }
        }
        output
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;
    use std::io::{BufRead, BufReader};

    use crate::utilities::tests::*;

    use super::*;

    #[test]
    fn check_size() {
        let data = load_test_file("deimos_game");
        let mut container = crate::Container::try_from(data.into_boxed_slice()).unwrap();

        container.load_sections().unwrap();
        let decoder = Decoder::from(&container.sections()[0]);
        let test_file = BufReader::new(get_test_file("deimos_code.txt"));
        assert_eq!(decoder.section.data().len() / 4, test_file.lines().count());
    }

    #[test]
    fn compare_output() {
        let data = load_test_file("deimos_game");
        let mut container = crate::Container::try_from(data.into_boxed_slice()).unwrap();

        container.load_sections().unwrap();
        let decoder = Decoder::from(&container.sections()[0]);
        let test_file = BufReader::new(get_test_file("deimos_code.txt"));

        for (i, line) in test_file.lines().enumerate() {
            let decoded = match decoder.decode_string(i) {
                Ok(v) => v,
                Err(_) => format!(".long {:#x}", decoder.get(i)),
            };
            assert_eq!(decoded, line.unwrap());
        }
    }
}
