use std::convert::TryFrom;

macro_rules! define_opcode {
    ( $e:ident, $f:ident, $bits:expr ) => {
        define_opcode!{$e, $f, $bits, 0}
    };
    ( $e:ident, $f:ident, $bits:expr, $shift:expr ) => {
        macro_rules! $e {
            ($m:expr) => {
                $m >> $shift & (2_u32.pow($bits) - 1)
            }
        }

        macro_rules! $f {
            ( $m:expr, 0 ) => {
                ($m >> $shift).trailing_zeros() >= $bits
            };
            ( $m:expr, $value:expr ) => {
                $e!($m) == $value
            };
        }
    };
}

macro_rules! opcode {
    ( $e:expr ) => {
        $e >> 26
    };
}

define_opcode! {a_opcode, a_opcode_eq, 5, 1}
define_opcode! {ds_opcode, ds_opcode_eq, 2}
define_opcode! {dx_opcode, dx_opcode_eq, 5, 1}
define_opcode! {md_opcode, md_opcode_eq, 3, 2}
//define_opcode! {mds_opcode, mds_opcode_eq, 4, 1}
define_opcode! {va_opcode, va_opcode_eq, 6}
define_opcode! {vx_opcode, vx_opcode_eq, 11}
define_opcode! {x_opcode, x_opcode_eq, 10, 1}
define_opcode! {xfx_opcode, xfx_opcode_eq, 10, 1}
define_opcode! {xo_opcode, xo_opcode_eq, 10, 1}
define_opcode! {xl_opcode, xl_opcode_eq, 10, 1}

macro_rules! create {
    ( A => $e:expr, $f:expr) => {{
        let (target, reg_a, reg_b, reg_c, rcd) = ISA::extract_a($f);
        $e(target, reg_a, reg_b, reg_c, rcd)
    }};
    ( A_NO_B => $e:expr, $f:expr) => {{
        create!(A => |a,b,_,d,e| $e(a,b,d,e), $f)
    }};
    ( A_NO_C => $e:expr, $f:expr) => {{
        create!(A => |a,b,c,_,e| $e(a,b,c,e), $f)
    }};
    ( B => ($m:expr,$n:expr, $o:expr, $p:expr), $f:expr) => {{
        let (options, test, immediate, absolute, link) = ISA::extract_b($f);
        match (absolute, link) {
            (false, false) => $m(options, test, immediate),
            (true, false) => $n(options, test, immediate),
            (false, true) => $o(options, test, immediate),
            (true, true) => $p(options, test, immediate),
        }
    }};
    ( D => $e:expr, $f:expr) => {{
        let (t_or_s, reg_a, immediate) = ISA::extract_d($f);
        $e(t_or_s, reg_a, immediate)
    }};
    ( DS => $e:expr, $f:expr) => {{
        let (target, reg_a, immediate) = ISA::extract_ds($f);
        $e(target, reg_a, immediate)
    }};
    ( DX => $e:expr, $f:expr) => {{
        let (target, immediate) = ISA::extract_dx($f);
        $e(target, immediate)
    }};
    ( I => ($m:expr,$n:expr, $o:expr, $p:expr), $f:expr) => {{
        let (immediate, absolute, link) = ISA::extract_i($f);
        match (absolute, link) {
            (false, false) => $m(immediate as i32),
            (true, false) => $n(immediate as i32),
            (false, true) => $o(immediate as i32),
            (true, true) => $p(immediate as i32),
        }
    }};
    ( M => $e:expr, $f:expr) => {{
        let (src, reg_a, op_b, mask_a, mask_b, rcd) = ISA::extract_m($f);
        $e(src, reg_a, op_b, mask_a, mask_b, rcd)
    }};
    ( MD => $e:expr, $f:expr) => {{
        let (src, reg_a, shift_amount, mask, rcd) = ISA::extract_md($f);
        $e(src, reg_a, shift_amount, mask, rcd)
    }};
    /*    ( MDS => $e:expr, $f:expr) => {{
        let (src, reg_a, reg_b, mask, rcd) = ISA::extract_mds($f);
        $e(src, reg_a, reg_b, mask, rcd)
    }};*/
    ( VA => $e:expr, $f:expr) => {{
        let (target, reg_a, reg_b, reg_c) = ISA::extract_va($f);
        $e(target, reg_a, reg_b, reg_c)
    }};
    ( VX => $e:expr, $f:expr) => {{
        let (t_or_s, op_a, op_b) = ISA::extract_vx($f);
        $e(t_or_s, op_a, op_b)
    }};
    ( VX_NO_TA => $e:expr, $f:expr) => {{
        create!(VX => |_,_,c| $e(c), $f)
    }};
    ( VX_NO_AB => $e:expr, $f:expr) => {{
        create!(VX => |a,_,_| $e(a), $f)
    }};
    ( X => $e:expr, $f:expr) => {{
        let (t_or_s, reg_a, reg_b, rcd) = ISA::extract_x($f);
        $e(t_or_s, reg_a, reg_b, rcd)
    }};
    ( X_NO_A => $e:expr, $f:expr) => {{
        create!(X => |a,_,c,d| $e(a,c,d), $f)
    }};
    ( X_NO_B => $e:expr, $f:expr) => {{
        create!(X => |a,b,_,d| $e(a,b,d), $f)
    }};
    ( X_NO_RCD => $e:expr, $f:expr) => {{
        create!(X => |a,b,c,_| $e(a,b,c), $f)
    }};
    ( XFX => ($x:expr, $y:expr), $f:expr) => {{
        let (target, special) = ISA::extract_xfx($f);
        if special & 0x200 != 0 {
            $y(target, (special >> 1) as u8)
        } else {
            $x(target, (special >> 1) as u8)
        }
    }};
    ( XFX => $e:expr, $f:expr) => {{
        let (target, special) = ISA::extract_xfx($f);
        $e(target, special)
    }};
    ( XL => ($x:expr, $y:expr), $f:expr) => {{
        let (options, test, hint, link) = ISA::extract_xl($f);
        if !link {
            $x(options, test, hint)
        } else {
            $y(options, test, hint)
        }
    }};
    ( XL => $e:expr, $f:expr) => {{
        let (target, reg_a, op, _) = ISA::extract_xl($f);
        $e(target, reg_a, op)
    }};
    ( XO => ($x:expr, $y:expr), $f:expr) => {{
        let (target, reg_a, reg_b, special, rcd) = ISA::extract_xo($f);
        if !special {
            $x(target, reg_a, reg_b, rcd)
        } else {
            $y(target, reg_a, reg_b, rcd)
        }
    }};
    ( XO => $e:expr, $f:expr) => {{
        let (target, reg_a, reg_b, _, rcd) = ISA::extract_xo($f);
        $e(target, reg_a, reg_b, rcd)
    }};
    ( XO_NO_B => ($x:expr, $y:expr), $f:expr) => {{
        let (target, reg_a, _, special, rcd) = ISA::extract_xo($f);
        if !special {
            $x(target, reg_a, rcd)
        } else {
            $y(target, reg_a, rcd)
        }
    }};
}

pub enum ISA {
    ADD(u8, u8, u8, bool),
    ADDC(u8, u8, u8, bool),
    ADDCO(u8, u8, u8, bool),
    ADDE(u8, u8, u8, bool),
    ADDEO(u8, u8, u8, bool),
    ADDI(u8, u8, i16),
    ADDIC(u8, u8, i16, bool),
    ADDIS(u8, u8, i16),
    ADDO(u8, u8, u8, bool),
    ADDPCIS(u8, u16),
    ADDZE(u8, u8, bool),
    ADDZEO(u8, u8, bool),
    AND(u8, u8, u8, bool),
    ANDI(u8, u8, u16),
    ANDIS(u8, u8, u16),
    ANDC(u8, u8, u8, bool),
    ATTN,
    B(i32),
    BA(i32),
    BL(i32),
    BLA(i32),
    BC(u8, u8, i32),
    BCA(u8, u8, i32),
    BCCTR(u8, u8, u8),
    BCL(u8, u8, i32),
    BCLA(u8, u8, i32),
    BCLR(u8, u8, u8),
    BCLRL(u8, u8, u8),
    CMP(u8, bool, u8, u8),
    CMPI(u8, bool, u8, i16),
    CMPL(u8, bool, u8, u8),
    CMPLI(u8, bool, u8, u16),
    CNTLZW(u8, u8, bool),
    CROR(u8, u8, u8),
    DIVW(u8, u8, u8, bool),
    DIVWO(u8, u8, u8, bool),
    DIVWU(u8, u8, u8, bool),
    DIVWUO(u8, u8, u8, bool),
    EXTSB(u8, u8, bool),
    EXTSH(u8, u8, bool),
    FABS(u8, u8, bool),
    FADD(u8, u8, u8, bool),
    FADDS(u8, u8, u8, bool),
    FCMPO(u8, u8, u8),
    FCMPU(u8, u8, u8),
    FCTIWZ(u8, u8, bool),
    FDIV(u8, u8, u8, bool),
    FDIVS(u8, u8, u8, bool),
    FMADD(u8, u8, u8, u8, bool),
    FMADDS(u8, u8, u8, u8, bool),
    FMR(u8, u8, bool),
    FMSUBS(u8, u8, u8, u8, bool),
    FMUL(u8, u8, u8, bool),
    FMULS(u8, u8, u8, bool),
    FNEG(u8, u8, bool),
    FNMADD(u8, u8, u8, u8, bool),
    FNMSUB(u8, u8, u8, u8, bool),
    FNMSUBS(u8, u8, u8, u8, bool),
    FRSP(u8, u8, bool),
    FSUB(u8, u8, u8, bool),
    FSUBS(u8, u8, u8, bool),
    LBZ(u8, u8, i16),
    LBZU(u8, u8, i16),
    LBZX(u8, u8, u8),
    LD(u8, u8, i16),
    LHZ(u8, u8, i16),
    LHZX(u8, u8, u8),
    LFD(u8, u8, i16),
    LFDU(u8, u8, u16),
    LFS(u8, u8, u16),
    LFSU(u8, u8, u16),
    LFSX(u8, u8, u8),
    LHA(u8, u8, i16),
    LHAX(u8, u8, u8),
    LMW(u8, u8, i16),
    LVEWX(u8, u8, u8),
    LVX(u8, u8, u8),
    LWARX(u8, u8, u8, bool),
    LWZ(u8, u8, i16),
    LWZU(u8, u8, i16),
    LWZX(u8, u8, u8),
    MFCR(u8),
    MFOCRF(u8, u8),
    MFSPR(u8, u16),
    MFVSCR(u8),
    MULHW(u8, u8, u8, bool),
    MULHWU(u8, u8, u8, bool),
    MULLI(u8, u8, u16),
    MULLW(u8, u8, u8, bool),
    MULLWO(u8, u8, u8, bool),
    MTCRF(u8, u8),
    MTOCRF(u8, u8),
    MTSPR(u8, u16),
    MTVSCR(u8),
    NEG(u8, u8, bool),
    NEGO(u8, u8, bool),
    NOR(u8, u8, u8, bool),
    OR(u8, u8, u8, bool),
    ORI(u8, u8, u16),
    ORIS(u8, u8, u16),
    RLDIC(u8, u8, u8, u8, bool),
    RLDICL(u8, u8, u8, u8, bool),
    RLDICR(u8, u8, u8, u8, bool),
    RLDIMI(u8, u8, u8, u8, bool),
    RLWIMI(u8, u8, u8, u8, u8, bool),
    RLWINM(u8, u8, u8, u8, u8, bool),
    SLW(u8, u8, u8, bool),
    SRAWI(u8, u8, u8, bool),
    SRW(u8, u8, u8, bool),
    STB(u8, u8, i16),
    STBU(u8, u8, i16),
    STBX(u8, u8, u8),
    STD(u8, u8, i16),
    STFD(u8, u8, i16),
    STFDU(u8, u8, u16),
    STFDX(u8, u8, u8),
    STFS(u8, u8, u16),
    STFSX(u8, u8, u8),
    STH(u8, u8, i16),
    STHBRX(u8, u8, u8),
    STHX(u8, u8, u8),
    STMW(u8, u8, i16),
    STVEWX(u8, u8, u8),
    STVX(u8, u8, u8),
    STW(u8, u8, i16),
    STWBRX(u8, u8, u8),
    STWCX(u8, u8, u8),
    STWU(u8, u8, i16),
    STWX(u8, u8, u8),
    SUBF(u8, u8, u8, bool),
    SUBFC(u8, u8, u8, bool),
    SUBFCO(u8, u8, u8, bool),
    SUBFE(u8, u8, u8, bool),
    SUBFEO(u8, u8, u8, bool),
    SUBFIC(u8, u8, i16),
    SUBFO(u8, u8, u8, bool),
    SUBFZE(u8, u8, bool),
    SUBFZEO(u8, u8, bool),
    SYNC(u8),
    TDI(u8, u8, i16),
    VADDUBM(u8, u8, u8),
    VMHRADDSHS(u8, u8, u8, u8),
    XOR(u8, u8, u8, bool),
    XORI(u8, u8, u16),
    XORIS(u8, u8, u16),
}

impl ISA {
    fn extract_a(value: u32) -> (u8, u8, u8, u8, bool) {
        let target = (value >> 21 & 0x1F) as u8;
        let src_a = (value >> 16 & 0x1F) as u8;
        let src_b = (value >> 11 & 0x1F) as u8;
        let src_c = (value >> 6 & 0x1F) as u8;
        let rcd = value & 0x1 != 0;

        (target, src_a, src_b, src_c, rcd)
    }

    fn extract_b(value: u32) -> (u8, u8, i32, bool, bool) {
        let options = (value >> 21 & 0x1F) as u8;
        let test = (value >> 16 & 0x1F) as u8;
        let immediate = match value >> 2 & 0x3FFF {
            v if v & 0x2000 != 0 => v | 0xFFFF_C000,
            v => v,
        } as i32;
        let absolute = value & 0x2 != 0;
        let link = value & 0x1 != 0;

        (options, test, immediate, absolute, link)
    }

    fn extract_d(value: u32) -> (u8, u8, u16) {
        let t_or_s = (value >> 21 & 0x1F) as u8;
        let src = (value >> 16 & 0x1F) as u8;
        let immediate = (value & 0xFFFF) as u16;

        (t_or_s, src, immediate)
    }

    fn extract_ds(value: u32) -> (u8, u8, i16) {
        let target = (value >> 21 & 0x3F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let immediate = value as i16 >> 2;

        (target, reg_a, immediate)
    }

    fn extract_dx(value: u32) -> (u8, u16) {
        let target = (value >> 21 & 0x1F) as u8;
        let immediate =
            ((value >> 6 & 0x3FF) << 6 | (value >> 16 & 0x1F) << 1 | value & 0x1) as u16;

        (target, immediate)
    }

    fn extract_i(value: u32) -> (i32, bool, bool) {
        let immediate = match value >> 2 & 0xFF_FFFF {
            v if v & 0x0080_0000 != 0 => v | 0xFF00_0000,
            v => v,
        } as i32;
        let absolute = (value & 0x2) != 0;
        let link = (value & 0x1) != 0;

        (immediate, absolute, link)
    }

    fn extract_m(value: u32) -> (u8, u8, u8, u8, u8, bool) {
        let src = (value >> 21 & 0x1F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let op_b = (value >> 11 & 0x1F) as u8;
        let mask_a = (value >> 6 & 0x1F) as u8;
        let mask_b = (value >> 1 & 0x1F) as u8;
        let rcd = value & 0x1 != 0;

        (src, reg_a, op_b, mask_a, mask_b, rcd)
    }

    fn extract_md(value: u32) -> (u8, u8, u8, u8, bool) {
        let source = (value >> 21 & 0x1F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let shift_amount = ((value >> 1 & 0x1) as u8) << 5 | (value >> 11 & 0x1F) as u8;
        let mask = (value >> 5 & 0x3F) as u8;
        let rcd = value & 0x1 != 0;

        (source, reg_a, shift_amount, mask, rcd)
    }

    /*    fn extract_mds(value: u32) -> (u8, u8, u8, u8, bool) {
        let source = (value >> 21 & 0x1F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let reg_b = (value >> 11 & 0x1F) as u8;
        let mask = (value >> 5 & 0x3F) as u8;
        let rcd = value >> 1 & 0x1 != 0;

        (source, reg_a, reg_b, mask, rcd)
    }*/

    fn extract_va(value: u32) -> (u8, u8, u8, u8) {
        let target = (value >> 21 & 0x1F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let reg_b = (value >> 11 & 0x1F) as u8;
        let reg_c = (value >> 6 & 0x1F) as u8;

        (target, reg_a, reg_b, reg_c)
    }

    fn extract_vx(value: u32) -> (u8, u8, u8) {
        let t_or_s = (value >> 21 & 0x1F) as u8;
        let op_a = (value >> 16 & 0x1F) as u8;
        let op_b = (value >> 11 & 0x1F) as u8;

        (t_or_s, op_a, op_b)
    }

    fn extract_x(value: u32) -> (u8, u8, u8, bool) {
        let t_or_s = (value >> 21 & 0x1F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let reg_b = (value >> 11 & 0x1F) as u8;
        let rcd = value & 0x1 != 0;

        (t_or_s, reg_a, reg_b, rcd)
    }

    fn extract_xfx(value: u32) -> (u8, u16) {
        let t_or_s = (value >> 21 & 0x1F) as u8;
        let operand = (value >> 11 & 0x3FF) as u16;

        (t_or_s, operand)
    }

    fn extract_xl(value: u32) -> (u8, u8, u8, bool) {
        let target = (value >> 21 & 0x1F) as u8;
        let op_a = (value >> 16 & 0x1F) as u8;
        let op_b = (value >> 11 & 0x1F) as u8;
        let link = value & 0x1 != 0;

        (target, op_a, op_b, link)
    }

    fn extract_xo(value: u32) -> (u8, u8, u8, bool, bool) {
        let target = (value >> 21 & 0x1F) as u8;
        let reg_a = (value >> 16 & 0x1F) as u8;
        let reg_b = (value >> 11 & 0x1F) as u8;
        let sp_reg = value >> 10 & 0x1 != 0;
        let rcd = value & 0x1 != 0;

        (target, reg_a, reg_b, sp_reg, rcd)
    }

    fn reverse_bytes(input: u16, byte_size: u16, max: u16) -> u16 {
        (0..max).step_by(byte_size as usize).fold(0, |acc, v| {
            let tmp = (input >> v) & (2u16.pow(byte_size.into()) - 1);
            acc | tmp << (max - byte_size - v)
        })
    }
}

impl TryFrom<u32> for ISA {
    type Error = super::error::Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match opcode!(value) {
            0 if x_opcode_eq!(value, 256) => Ok(Self::ATTN),
            2 => Ok(create!(D => |a,b,c| Self::TDI(a,b,c as i16), value)),
            4 if vx_opcode_eq!(value, 0) => Ok(create!(VX => Self::VADDUBM, value)),
            4 if va_opcode_eq!(value, 33) => Ok(create!(VA => Self::VMHRADDSHS, value)),
            4 if vx_opcode_eq!(value, 1540) => Ok(create!(VX_NO_AB => Self::MFVSCR, value)),
            4 if vx_opcode_eq!(value, 1604) => Ok(create!(VX_NO_TA => Self::MTVSCR, value)),
            7 => Ok(create!(D => Self::MULLI, value)),
            8 => Ok(create!(D => |a,b,c| Self::SUBFIC(a,b,c as i16), value)),
            10 => Ok(create!(D => |a,b,c| Self::CMPLI(a >> 2, a & 0x1 != 0, b, c), value)),
            11 => Ok(create!(D => |a,b,c| Self::CMPI(a >> 2, a & 0x1 != 0, b, c as i16), value)),
            t @ 12 | t @ 13 => {
                Ok(create!(D => |a,b,c| Self::ADDIC(a, b, c as i16, t == 13), value))
            }
            14 => Ok(create!(D => |a,b,c| Self::ADDI(a,b,c as i16), value)),
            15 => Ok(create!(D => |a,b,c| Self::ADDIS(a,b,c as i16), value)),
            16 => Ok(create!(B => (Self::BC, Self::BCA, Self::BCL, Self::BCLA), value)),
            18 => Ok(create!(I => (Self::B, Self::BA, Self::BL, Self::BLA), value)),
            19 if xl_opcode_eq!(value, 16) => Ok(create!(XL => (Self::BCLR, Self::BCLRL), value)),
            19 if xl_opcode_eq!(value, 449) => Ok(create!(XL => Self::CROR, value)),
            19 if xl_opcode_eq!(value, 528) => Ok(create!(XL => Self::BCCTR, value)),
            19 if dx_opcode_eq!(value, 2) => Ok(create!(DX => Self::ADDPCIS, value)),
            20 => Ok(create!(M => Self::RLWIMI, value)),
            21 => Ok(create!(M => Self::RLWINM, value)),
            24 => Ok(create!(D => Self::ORI, value)),
            25 => Ok(create!(D => Self::ORIS, value)),
            26 => Ok(create!(D => Self::XORI, value)),
            27 => Ok(create!(D => Self::XORIS, value)),
            28 => Ok(create!(D => Self::ANDI, value)),
            29 => Ok(create!(D => Self::ANDIS, value)),
            30 if md_opcode_eq!(value, 0) => {
                Ok(create!(MD => |a,b,c,d,e| Self::RLDICL(a,b,c, (d & 0x1) << 5 | d >> 1,e), value))
            }
            30 if md_opcode_eq!(value, 1) => Ok(create!(MD => Self::RLDICR, value)),
            30 if md_opcode_eq!(value, 2) => {
                Ok(create!(MD => |a,b,c,d,e| Self::RLDIC(a,b,c, (d & 0x1) << 5 | d >> 1,e), value))
            }
            30 if md_opcode_eq!(value, 3) => Ok(create!(MD => Self::RLDIMI, value)),
            31 if x_opcode_eq!(value, 0) => Ok(create!( X =>
                |a, b, c, _| Self::CMP(a >> 2, a & 0x1 != 0, b, c),
                value
            )),
            31 if xo_opcode_eq!(value, 8) => Ok(create!(XO => (Self::SUBFC, Self::SUBFCO), value)),
            31 if xo_opcode_eq!(value, 10) => Ok(create!(XO => (Self::ADDC, Self::ADDCO), value)),
            31 if xo_opcode_eq!(value, 11) => Ok(create!(XO => Self::MULHWU, value)),
            31 if x_opcode_eq!(value, 19) => {
                Ok(create!(XFX => (|t,_| Self::MFCR(t), Self::MFOCRF), value))
            }
            31 if x_opcode_eq!(value, 20) => Ok(create!(X => Self::LWARX, value)),
            31 if x_opcode_eq!(value, 23) => Ok(create!(X_NO_RCD => Self::LWZX, value)),
            31 if x_opcode_eq!(value, 24) => Ok(create!(X => Self::SLW, value)),
            31 if x_opcode_eq!(value, 26) => Ok(create!(X_NO_B => Self::CNTLZW, value)),
            31 if x_opcode_eq!(value, 28) => Ok(create!(X => Self::AND, value)),
            31 if x_opcode_eq!(value, 32) => {
                Ok(create!(X => |a,b,c,_| Self::CMPL(a >> 2, a & 0x1 != 0, b, c), value))
            }
            31 if xo_opcode_eq!(value, 40) => Ok(create!(XO => (Self::SUBF, Self::SUBFO), value)),
            31 if x_opcode_eq!(value, 60) => Ok(create!(X => Self::ANDC, value)),
            31 if x_opcode_eq!(value, 71) => Ok(create!(X_NO_RCD => Self::LVEWX, value)),
            31 if xo_opcode_eq!(value, 75) => Ok(create!(XO => Self::MULHW, value)),
            31 if x_opcode_eq!(value, 87) => Ok(create!(X_NO_RCD => Self::LBZX, value)),
            31 if x_opcode_eq!(value, 103) => Ok(create!(X_NO_RCD => Self::LVX, value)),
            31 if xo_opcode_eq!(value, 104) => {
                Ok(create!(XO_NO_B => (Self::NEG, Self::NEGO), value))
            }
            31 if x_opcode_eq!(value, 124) => Ok(create!(X => Self::NOR, value)),
            31 if xo_opcode_eq!(value, 136) => {
                Ok(create!(XO => (Self::SUBFE, Self::SUBFEO), value))
            }
            31 if xo_opcode_eq!(value, 138) => Ok(create!(XO => (Self::ADDE, Self::ADDEO), value)),
            31 if x_opcode_eq!(value, 144) => {
                Ok(create!(XFX => (Self::MTCRF, Self::MTOCRF), value))
            }
            //Record bit needs to be set
            31 if x_opcode_eq!(value, 150) && value & 0x1 != 0 => {
                Ok(create!(X => |a,b,c,_| Self::STWCX(a, b, c), value))
            }
            31 if x_opcode_eq!(value, 151) => Ok(create!(X_NO_RCD => Self::STWX, value)),
            31 if x_opcode_eq!(value, 199) => Ok(create!(X_NO_RCD => Self::STVEWX, value)),
            31 if xo_opcode_eq!(value, 200) => {
                Ok(create!(XO_NO_B => (Self::SUBFZE, Self::SUBFZEO), value))
            }
            31 if xo_opcode_eq!(value, 202) => {
                Ok(create!(XO_NO_B => (Self::ADDZE, Self::ADDZEO), value))
            }
            31 if x_opcode_eq!(value, 215) => Ok(create!(X_NO_RCD => Self::STBX, value)),
            31 if x_opcode_eq!(value, 231) => Ok(create!(X_NO_RCD => Self::STVX, value)),
            31 if xo_opcode_eq!(value, 235) => {
                Ok(create!(XO => (Self::MULLW, Self::MULLWO), value))
            }
            31 if xo_opcode_eq!(value, 266) => Ok(create!(XO => (Self::ADD, Self::ADDO), value)),
            31 if x_opcode_eq!(value, 279) => Ok(create!(X_NO_RCD => Self::LHZX, value)),
            31 if x_opcode_eq!(value, 316) => Ok(create!(X => Self::XOR, value)),
            31 if xfx_opcode_eq!(value, 339) => Ok(create!(XFX => |t, spr| {
                Self::MFSPR(t, Self::reverse_bytes(spr, 5, 10))
            }, value)),
            31 if x_opcode_eq!(value, 343) => Ok(create!(X_NO_RCD => Self::LHAX, value)),
            31 if xfx_opcode_eq!(value, 371) => {
                Ok(create!(XFX => |a,b|Self::MFSPR(a,Self::reverse_bytes(b,5,10)), value))
            }
            31 if x_opcode_eq!(value, 407) => Ok(create!(X_NO_RCD => Self::STHX, value)),
            31 if x_opcode_eq!(value, 444) => Ok(create!(X => Self::OR, value)),
            31 if xo_opcode_eq!(value, 459) => {
                Ok(create!(XO => (Self::DIVWU, Self::DIVWUO), value))
            }
            31 if xfx_opcode_eq!(value, 467) => Ok(create!(XFX => |t, spr| {
            Self::MTSPR(t, Self::reverse_bytes(spr,5,10))
            }, value)),
            31 if xo_opcode_eq!(value, 491) => Ok(create!(XO => (Self::DIVW, Self::DIVWO), value)),
            31 if x_opcode_eq!(value, 535) => Ok(create!(X_NO_RCD => Self::LFSX, value)),
            31 if x_opcode_eq!(value, 536) => Ok(create!(X => Self::SRW, value)),
            31 if x_opcode_eq!(value, 598) => {
                Ok(create!(X => |a,_,_,_| Self::SYNC(a & 0x3), value))
            }
            31 if x_opcode_eq!(value, 662) => Ok(create!(X_NO_RCD => Self::STWBRX, value)),
            31 if x_opcode_eq!(value, 663) => Ok(create!(X_NO_RCD => Self::STFSX, value)),
            31 if x_opcode_eq!(value, 727) => Ok(create!(X_NO_RCD => Self::STFDX, value)),
            31 if x_opcode_eq!(value, 824) => Ok(create!(X => Self::SRAWI, value)),
            31 if x_opcode_eq!(value, 918) => Ok(create!(X_NO_RCD => Self::STHBRX, value)),
            31 if x_opcode_eq!(value, 922) => Ok(create!(X_NO_B => Self::EXTSH, value)),
            31 if x_opcode_eq!(value, 954) => Ok(create!(X_NO_B => Self::EXTSB, value)),
            32 => Ok(create!(D => |a,b,c| Self::LWZ(a,b,c as i16), value)),
            33 => Ok(create!(D => |a,b,c| Self::LWZU(a,b,c as i16), value)),
            34 => Ok(create!(D => |a,b,c| Self::LBZ(a,b,c as i16), value)),
            35 => Ok(create!(D => |a,b,c| Self::LBZU(a,b,c as i16), value)),
            36 => Ok(create!(D => |a,b,c| Self::STW(a,b,c as i16), value)),
            37 => Ok(create!(D => |a,b,c| Self::STWU(a,b,c as i16), value)),
            38 => Ok(create!(D => |a,b,c| Self::STB(a,b,c as i16), value)),
            39 => Ok(create!(D => |a,b,c| Self::STBU(a,b,c as i16), value)),
            40 => Ok(create!(D => |a,b,c| Self::LHZ(a,b,c as i16), value)),
            42 => Ok(create!(D => |a,b,c| Self::LHA(a,b,c as i16), value)),
            44 => Ok(create!(D => |a,b,c| Self::STH(a,b,c as i16), value)),
            46 => Ok(create!(D => |a,b,c| Self::LMW(a,b,c as i16), value)),
            47 => Ok(create!(D => |a,b,c| Self::STMW(a,b,c as i16), value)),
            48 => Ok(create!(D => Self::LFS, value)),
            49 => Ok(create!(D => Self::LFSU, value)),
            50 => Ok(create!(D => |a,b,c| Self::LFD(a,b,c as i16), value)),
            51 => Ok(create!(D => Self::LFDU, value)),
            52 => Ok(create!(D => Self::STFS, value)),
            54 => Ok(create!(D => |a,b,c| Self::STFD(a,b,c as i16), value)),
            55 => Ok(create!(D => Self::STFDU, value)),
            58 if ds_opcode_eq!(value, 0) => Ok(create!(DS => Self::LD, value)),
            59 if a_opcode_eq!(value, 18) => Ok(create!(A_NO_C => Self::FDIVS, value)),
            59 if a_opcode_eq!(value, 20) => Ok(create!(A_NO_C => Self::FSUBS, value)),
            59 if a_opcode_eq!(value, 21) => Ok(create!(A_NO_C => Self::FADDS, value)),
            59 if a_opcode_eq!(value, 25) => Ok(create!(A_NO_B => Self::FMULS, value)),
            59 if a_opcode_eq!(value, 28) => Ok(create!(A => Self::FMSUBS, value)),
            59 if a_opcode_eq!(value, 29) => Ok(create!(A => Self::FMADDS, value)),
            59 if a_opcode_eq!(value, 30) => Ok(create!(A => Self::FNMSUBS, value)),
            62 if ds_opcode_eq!(value, 0) => Ok(create!(DS => Self::STD, value)),
            63 if x_opcode_eq!(value, 0) => create!(X => |a: u8,b,c,_| {
                    if a.trailing_zeros() >= 2 {
                        Ok(Self::FCMPU(a >> 2, b, c))
                    } else {
                        Err(Self::Error::IllegalOperand("Reserved fields contain non-zero value".to_string()))
                    }}, value),
            63 if x_opcode_eq!(value, 12) => Ok(create!(X_NO_A => Self::FRSP, value)),
            63 if x_opcode_eq!(value, 15) => Ok(create!(X_NO_A => Self::FCTIWZ, value)),
            63 if a_opcode_eq!(value, 18) => Ok(create!(A_NO_C => Self::FDIV, value)),
            63 if a_opcode_eq!(value, 20) => Ok(create!(A_NO_C => Self::FSUB, value)),
            63 if a_opcode_eq!(value, 21) => Ok(create!(A_NO_C => Self::FADD, value)),
            63 if a_opcode_eq!(value, 25) => Ok(create!(A_NO_B => Self::FMUL, value)),
            63 if a_opcode_eq!(value, 29) => Ok(create!(A => Self::FMADD, value)),
            63 if a_opcode_eq!(value, 30) => Ok(create!(A => Self::FNMSUB, value)),
            63 if a_opcode_eq!(value, 31) => Ok(create!(A => Self::FNMADD, value)),
            63 if x_opcode_eq!(value, 32) => {
                Ok(create!(X => |a,b,c,_| Self::FCMPO(a >> 2, b, c), value))
            }
            63 if x_opcode_eq!(value, 40) => Ok(create!(X_NO_A => Self::FNEG, value)),
            63 if x_opcode_eq!(value, 72) => Ok(create!(X_NO_A => Self::FMR, value)),
            63 if x_opcode_eq!(value, 264) => Ok(create!(X_NO_A => Self::FABS, value)),
            v => Err(Self::Error::UnknownOpcode(v as u8)),
        }
    }
}
